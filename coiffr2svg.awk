#!/usr/bin/awk -f
BEGIN{
	print "<svg>"
	print "<style type='text/css' >\n<![CDATA[\n text {\nfont-family: mono;\n}\n]]>\n</style>"
	width = 400
	pad = 300
    gap = 30
    times = 1
}
{
	match($0, /seq=([^;]+);query=([^;]+);aln=([^;]+);.*;before_end=([^;]+);/, a)
	name = $1
	from = $4
	to = $5
	seq = a[1]
	query = a[2]
	aln = a[3]
	end = a[4]
	len = to+end
	if(last != name) {
        if(last != "") {
            printf("<text x='-1' y='%d' text-anchor='end'>%d/%d = %.1f targets/1000nt</text>", count*50+20, times-1, last_len, (times-1)/last_len*1000)
        }
        count++
        times = 1
        space = len + gap
        print "<text x='-1' y='"(count*50+8)"' text-anchor='end'>"name"</text>"
        print "<rect x='0' y='"count*50"' height='20' width='"len"' fill='#fff' stroke='#000'/>"
        #print "<rect x='"pad"' y='"count*50"' height='20' width='1' stroke='#999'/>"
        #print "<rect x='"len-pad"' y='"count*50"' height='20' width='1' stroke='#999'/>"
    }
    print "<rect x='"from"' y='"count*50+1"' height='18' width='"to-from"' fill='#f00'/>"
    print "<text x='"from"' y='"count*50-4"'>"times"</text>"

    print "<text x='"space-13"' y='"count*50+12"'>"times"=</text>"
    print "<text x='"space"' y='"(count*50)"' xml:space='preserve'>"seq"</text>"
    print "<text x='"space"' y='"(count*50+12)"' xml:space='preserve'>"aln"</text>"
    print "<text x='"space"' y='"(count*50+26)"' xml:space='preserve'>"query"</text>"
    #for(i=1; i<=length(seq); i++) {
    #    print "<text x='"(space+i*8)"' y='"(count*50)"'>"substr(seq, i, 1)"</text>"
    #    print "<text x='"(space+i*8)"' y='"(count*50+12)"'>"substr(aln, i, 1)"</text>"
    #    print "<text x='"(space+i*8)"' y='"(count*50+26)"'>"substr(query, i, 1)"</text>"
    #}
    space = space + length(seq) * 8 + gap
	last = name
    last_len = len
    times++
}
END{
    if(name != "") {
        printf("<text x='-1' y='%d' text-anchor='end'>%d/%d = %.1f targets/1000nt</text>", count*50+20, times-1, len, (times-1)/len*1000)
    }
	print "</svg>"
}
