# coiffr - Complementary Oligonucleotide Interaction Finder For RNA
This tools searches for regions in the reference FASTA that are complementary to the query sequence. Allowing wobble base pairs between G:U, insertions, deletions and substitutions (errors).

## Requirements
- Python 3
- [regex](https://pypi.org/project/regex/): a third party regex library for python that can handle mismatches. `pip install regex`
- [BioPython](https://pypi.org/project/biopython/): a mighty library for many bioinformatic things. Here used just for robust `FASTA` parsing. `pip install biopython`
- (optionally) `RNAcofold` from the [ViennaRNA Package](https://www.tbi.univie.ac.at/RNA/#download) ([GitHub](https://github.com/ViennaRNA/ViennaRNA)): if you want predicted binding energies of potential interaction sites as part of the output (`-F`).

## Usage
        coiffr [-h] [-r] [-e N] [-i N] [-d N] [-s N] [-w] [-m N] [-F] [-v] fasta query

        positional arguments:
            fasta                 FASTA file of reference genome
            query                 oligonucleotide sequence (use "N" as wildcard

        optional arguments:
            -h, --help            show this help message and exit
            -r, --revcomp         also search in reverse complement of reference (used
                                  for similar sequence searches)
            -e N, --errors N      max. allowed errors
            -i N, --inserts N     max. allowed inserts
            -d N, --deletions N   max. allowed deletions
            -s N, --substitutions N  max. allowed substitutions
            -w, --wobble          allow wobble base pairs
            -m N, --mirna N       miRNA target prediction mode with min. N matches
            -F, --fold            include free energy score
            -v, --verbose         print verbose output to STDERR

## Auxiliary scripts
By default `coiffr` will give `GFF` style output with one line per match. If you are interested in how many matches each reference sequence (e.g. in a transcriptome) received, this output can be converted by the `coiffr2tsv.awk` script:

        ./coiffr.py ref.transcripts.fasta AGTCGAAAAAGA | ./coiffr2tsv.awk > stats.tsv

For quick visualization of the same statistics you can run the `coiffr2svg.awk` script that generates a `SVG` graphics file with a box for each transcript highlighting positions of matches in red and writing information around it. The output is not very sophisticated and might produce extremely large `SVG`s in both dimensions.

        ./coiffr.py ref.transcripts.fasta AGTCGAAAAAGA | ./coiffr2svg.awk > matches.svg
