#!/usr/bin/awk -f
BEGIN{
	width = 400
	pad = 300
    gap = 30
    times = 1
	print "#name\tlength\thits\thits_per_1000nt\tpositions"
}
{
	match($0, /seq=([^;]+);query=([^;]+);aln=([^;]+);.*;before_end=([^;]+);/, a)
	name = $1
	len = $5+a[4]
	if(last != name) {
        if(last != "") {
            printf("%s\t%d\t%d\t%.1f\t%s\n", last, last_len, times, times/last_len*1000, positions)
        }
        times = 0
        positions = ""$4
    } else {
        positions = positions","$4
    }
	last = name
    last_len = len
    times++
}
END{
    if(name != "") {
        printf("%s\t%d\t%d\t%.1f\t%s\n", last, len, times, times/len*1000, positions)
    }
}
